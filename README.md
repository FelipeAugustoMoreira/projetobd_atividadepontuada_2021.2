# Smart Organic
##### Projeto das disciplinas Banco de dados 2 e Gestão de Configuração e Mudanças

## Equipes
#### Em Banco de Dados 2
- Ary Sault
- Bruna Ribeiro
- Dmitry Rocha
- Felipe Augusto
- Lucas Bonafini

#### Em Gestão de Configuração e Mudanças
- Ary Sault
- Dmitry Rocha
- Luis Felipe Gomes
- Jamile Conceição 

## Escopo 
Fornecer um banco de dados que gerencie clientes, produtos e rotas de entrega para uma empresa de produtos orgânicos que atua na região metropolitana de Salvador<br/>

## Visão do produto 
**Para** uma empresa de entrega de produtos orgânicos que atua na região metropolitana de Salvador<br/>
**Quê** precisa de um sistema de vendas e cadastro de clientes <br/>
**O** Smart Organic<br/>
**É um** sistema Web<br/>
**Quê** gerencia produtos, clientes e vendas<br/>
**Diferente do** Nuvemshop, VirtUOL, Televendas Fácil, entre outras plataformas de vendas.<br/>
**Nosso produto** ajuda a agilizar o processo de controle e vendas; torna flexível o controle de clientes e otimiza as rotas de entrega<br/>
